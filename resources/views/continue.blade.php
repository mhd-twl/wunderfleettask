@extends('master')
@section('content')
	<div class="row">
	    <div class="col-md-12">
	      <div class="panel-default no-bd">
	        <div class="panel-header">
	          <h2 class="panel-title">Continue Your Registration..</h2>
	          <h3> By enter your registered telephone</h3>
	        </div>
	        <div class="panel-body bg-white">
	          <div class="row">
	            <div class="col-md-12 col-sm-12 col-xs-12">
	            	<form method="post" action="{{ route('continue_customer') }}" > 
						<div class="row">
						{{ csrf_field() }}
						{{ Form::input('text','telephone',null,
					          ['class' => 'form-control required' ,@autofocus , @required
					          ,'placeholder'=>'please enter your registered telephone number...'
					          ])}}
						</div>
						<br>
						<div class="row"> 
							<button class="btn pull-right btn-success"> Send </button>
						 </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
