@extends('master')
@section('content')
	<div class="row">
		<div class="col-md-8">
			 <div class="row">
			 	@if(Session::has('cur_customer'))
					<p class="alert alert-info" style="overflow-wrap: break-word;">
						Your Inserted Data:
					{{ Session::get('cur_customer') }}</p>
				@endif
	        	@if(Session::has('message'))
					<p class="alert {{Session::get('alert-class') }}">
					{{ Session::get('message') }} {{  Session::forget('message')  }}</p>
				@endif
	        	</div>
		</div>
	    <div class="col-md-12">
	      <div class="panel-default no-bd">
	        <div class="alert alert-success">
	          <h2 class="panel-title"> Successful Registration  
							<h5 class="alert"> Please hold your Payment ID: </h5>
							<h5 class="alert alert-success">  {{  $customer->paymentDataId }} </h5>
						</h2>
	        </div>
	        <div class="panel-body bg-white">
	       
	        <div class="row"> &nbsp; </div>
	          <div class="row">
	            <div class="col-md-12 col-sm-12 col-xs-12">
	           			  <div class="row"> 
							Thanks for your time...
						</div>
						<br> <br>
	            	<div class="row"> 
	            		<a href="{{ url('/') }}" class="btn pull-left btn-default" title="This allows you to continue it later!" > Go Home </a> 
					</div>
            	</div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection


 
