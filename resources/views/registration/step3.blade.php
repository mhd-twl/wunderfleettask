@extends('registration.stepform' , ['step_number' => 3 , 'step_title' => 'Payment Data' ])
@section('step_form')
	<div class="row">
	  <div class="col-sm-4">
	    <div class="form-group">
	       {{ Form::label('account_owner' , 'Account owner name *' )}}
	        {{ Form::input('text','account_owner',null,
	          ['class' => 'form-control required' ,@autofocus , @required
	          ,'placeholder'=>' Enter the account owner name '
	          ])}}
	    </div>
	  </div>
	   <div class="col-sm-4">
	    <div class="form-group">
	       {{ Form::label('iban' , ' IBAN Code *' )}}
	        {{ Form::input('text','iban',null,
	          ['class' => 'form-control required' ,@autofocus , @required
	          ,'placeholder'=>' Enter your iban '
	          ])}}
	    </div>
	  </div>
	</div>
@endsection


 