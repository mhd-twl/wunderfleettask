@extends('registration.stepform' , ['step_number' => 2 , 'step_title' => 'Address Data' ])
@section('step_form')
	<div class="row">
	   <div class="col-sm-4">
	    <div class="form-group">
	       {{ Form::label('city' , 'City' )}}
				 <select  autofocus  required name="city" class="form-control required">
				 		<option value="damascus" > damascus </option>
						 <option value="berlin" > berlin </option>
					</select>
	    </div>
	  </div>
	  	<div class="col-sm-4">
	    <div class="form-group">
	       {{ Form::label('address' , 'address' )}}
	        {{ Form::input('text','address',null,
	          ['class' => 'form-control required' ,@autofocus , @required
	          ,'placeholder'=>' Enter your address '
	          ])}}
	    </div>
	  </div>
	   <div class="col-sm-4">
	    <div class="form-group">
	       {{ Form::label('street' , 'Street' )}}
	        {{ Form::input('text','street',null,
	          ['class' => 'form-control required' ,@autofocus , @required
	          ,'placeholder'=>' Enter your street '
	          ])}}
	    </div>
	  </div>
	    <div class="col-sm-4">
	    <div class="form-group">
	       {{ Form::label('house_no' , 'House No' )}}
	        {{ Form::input('text','house_no',null,
	          ['class' => 'form-control required' ,@autofocus , @required
	          ,'placeholder'=>' Enter your house no '
	          ])}}
	    </div>
	  </div>
	  	  <div class="col-sm-4">
	    <div class="form-group">
	       {{ Form::label('zip_code' , 'Zip code' )}}
	        {{ Form::input('text','zip_code',null,
	          ['class' => 'form-control required' ,@autofocus , @required
	          ,'placeholder'=>' Enter your zip city code '
	          ])}}
	    </div>
	  </div>
	</div>
@endsection


 