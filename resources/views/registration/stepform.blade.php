@extends('master')
@section('content')
	<div class="row">
		<div class="col-md-8">
			 <div class="row">
			 	@if(Session::has('cur_customer'))
					<p class="alert alert-info" style="overflow-wrap: break-word;">
						Your Inserted Data:
					{{ Session::get('cur_customer') }}</p>
				@endif
	        	@if(Session::has('message'))
					<p class="alert {{Session::get('alert-class') }}">
					{{ Session::get('message') }} {{  Session::forget('message')  }}</p>
				@endif
	        	</div>
		</div>
	    <div class="col-md-12">
	      <div class="panel-default no-bd">
	        <div class="panel-header">
	          <h2 class="panel-title">Registration | 
	          	<strong> Step {{$step_number}} / 3 </strong></h2>
	          <h3>{{ $step_title}} </h3>
	        </div>
	        <div class="panel-body bg-white">
	       
	        <div class="row"> &nbsp; </div>
	          <div class="row">
	            <div class="col-md-12 col-sm-12 col-xs-12">
	            	<form method="post" action="{{ route('register',[$step_number+1]) }}" > 
	            		{{ csrf_field() }}
	            		@yield('step_form')
	            		<div class="row"> 
	            			<input type="hidden" name="last_step" value="{{$step_number}}">
							<input type="submit" value="Next >> " 
										class="btn pull-right btn-primary"> 
						</div>
	            	</form>
	            	<div class="row"> 
						<a href="{{ url('/cancel') }}" class="btn pull-left btn-warning" title="Cancelation this resgistration!" > Cancel  </a>
						<a href="{{ url('/') }}" class="btn pull-left btn-default" title="This allows you to continue it later!" > leave now ... </a>

					</div>
            	</div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection


 
