@extends('registration.stepform' , ['step_number' => 1 , 'step_title' => 'Personal Data' ])
@section('step_form')
			<div class="row">
			  <div class="col-sm-4">
			    <div class="form-group">
			       {{ Form::label('firstname' , 'First name *' )}}
			        {{ Form::input('text','firstname',null,
			          ['class' => 'form-control required' ,@autofocus , @required
			          ,'placeholder'=>' Enter your firstname '
			          ])}}
			    </div>
			  </div>
			   <div class="col-sm-4">
			    <div class="form-group">
			       {{ Form::label('lastname' , 'Last name *' )}}
			        {{ Form::input('text','lastname',null,
			          ['class' => 'form-control required' ,@autofocus , @required
			          ,'placeholder'=>' Enter your lastname '
			          ])}}
			    </div>
			  </div>
			   <div class="col-sm-4">
			    <div class="form-group">
			       {{ Form::label('telephone' , 'Telephone *' )}}
			        {{ Form::input('text','telephone',null,
			          ['class' => 'form-control required' ,@autofocus , @required
			          ,'placeholder'=>' Enter your telephone '
			          ])}}
			    </div>
			  </div>
			</div>
@endsection


 