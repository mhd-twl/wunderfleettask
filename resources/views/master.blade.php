<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wunderfleet Task Oct 2018</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    
    <link href="/css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <br>
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            Wunderfleet Task By Mohamed Al Tawil.
        </div>
        <br><hr><br>
        <div class="col-md-12">
            @yield('content')
        </div>
    </div>
    <style>
        .ftr{
            bottom: 0px;
        }
    </style>
    </div>
     <br><hr><br>
    <footer class="row ftr pull-right">
        <div class="col-md-12">
            <small> <i> Done in Oct 2018. </i>  </small>
        </div>
    </footer>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

  </body>
</html>