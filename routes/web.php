<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {   return view('index'); });
Route::get('/continue', function () { return view('continue'); } )->name('continue');
Route::get('/continue_customer', 'CustomerController@continue')->name('continue_customer');

Route::get('/cancel', 'CustomerController@cancel')->name('cancel');


Route::get('/register/{step}', 'CustomerController@register')->name('register');
Route::post('/register/{step}', 'CustomerController@register')->name('register');


Route::get('/test', 'CustomerController@test')->name('test');




