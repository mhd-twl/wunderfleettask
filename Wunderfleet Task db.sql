-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for wunderfleet_db
CREATE DATABASE IF NOT EXISTS `wunderfleet_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `wunderfleet_db`;

-- Dumping structure for table wunderfleet_db.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_owner` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iban` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymentDataId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_step` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wunderfleet_db.customers: 2 rows
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT IGNORE INTO `customers` (`id`, `firstname`, `lastname`, `telephone`, `address`, `street`, `house_no`, `zip_code`, `city`, `account_owner`, `iban`, `paymentDataId`, `last_step`, `status`, `created_at`, `updated_at`) VALUES
	(8, 'test', 'test1', '123', 'test', 'test', 'test', '123123', 'damascus', 'test', '1231211', '71bc703f5c5d7209dab723abce733351851b192e37f229a1e36febccd7e3169facf753061b5ad1ce767bdcb6a7914f7e', 3, 1, '2018-10-07 11:27:43', '2018-10-07 11:27:44'),
	(7, 'Mohamed', 'AlTawil', '123123', '3123123', '12312312', '312312312', '3123123123', 'berlin', 'Mohamed33', '123123123123', '837c27c5487a3ae45db5f323774079a2b1d48c627c0f05be362c7ae6da6a0b0fb4af916289e42152d4934d8b9a8df855', 3, 1, '2018-10-07 11:19:49', '2018-10-07 11:19:50'),
	(5, 'Mohamed', 'Tawil', '123123123', '123', '123', '123', '123', 'berlin', 'Mohamed', '1232345123', '2bf6aee9533f886400a4f8a27dce7dab2d0772d2bea2aeb851a85cd565cebbc97a984e482107c53dc79aeea077de773c', 3, 1, '2018-10-07 11:12:35', '2018-10-07 11:17:03');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table wunderfleet_db.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wunderfleet_db.migrations: 1 rows
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2018_10_06_000000_create_customers_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
