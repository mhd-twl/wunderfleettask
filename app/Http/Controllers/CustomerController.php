<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Session;

class CustomerController extends Controller
{

    private $api_url = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";

    public function cancel()
    {
        Session()->forget('cur_customer');
        return redirect('/');
    }
    public function continue( Request $request )
    {
        $customer = Session::get('cur_customer');
        $step = 1;
        if($customer)
        { 
            $step = $customer->last_step  ;
            $step = (int) $step + 1 ; 
            return view('registration.step' . $step , compact('customer'));
        }else{
            return  $this->register(1, $request );
        }
    }
    public function register($step = 1 , Request $request )
    {
        
        $customer = null ;
        switch ($step) {
            case '1':
                 Session::forget('cur_customer'); 
                break;
            case '2':
                if($request->firstname)
                {
                   $customer = new Customer;
                   $customer->status = 0;
                   $customer->firstname = $request->firstname ;
                   $customer->lastname = $request->lastname ;
                   $customer->telephone = $request->telephone ;
                   $customer->last_step = $request->last_step ;
                   Session::put('cur_customer', $customer);
                }
                break;

            case '3':   
                if($request->address)
                {
                    $customer = Session::get('cur_customer');
                    $customer->city  = $request->city  ;
                    $customer->address  = $request->address  ;
                    $customer->street  = $request->street  ;
                    $customer->house_no  = $request->house_no  ;
                    $customer->zip_code  = $request->zip_code  ;
                    $customer->last_step = $request->last_step ;
                    Session::put('cur_customer', $customer);
                }
                break;

            case '4':
                if($request->account_owner)
                {
                    $account_owner  = $request->account_owner  ;
                    $iban  = $request->iban  ;
                    //----------
                    $customer = Session::get('cur_customer');
                    $customer->account_owner  = $account_owner  ;
                    $customer->iban  = $iban  ;
                    $customer->last_step = $request->last_step ;
                    $customer->save();

                    $customer->paymentDataId = 
                        $this->get_payment_id( $customer ,$iban  ,$account_owner );
                    if($customer->paymentDataId == "0")
                    {
                        Session::flash('message', ' Error while returning Payment id'); 
                        Session::flash('alert-class', 'alert-danger');
                        return view('registration.step' . 3 , compact('customer'));
                    }else{
                        $customer->status = 1; 
                        $customer->save();
                    }
                    Session::put('cur_customer', $customer);
                }
                break;
        }
 		return view('registration.step' . $step , compact('customer'));
    }
 
    private function call_payment_api_json( $method, $url, $postData = false)
    {
        $ch = curl_init ();
        $timeout = 0; // 100; // set to zero for no timeout
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, 1);
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json' ));
        curl_setopt ( $ch, CURLOPT_HEADER, 0 );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode($postData));  
        $file_contents = curl_exec ( $ch );
        if (curl_errno ( $ch )) {
            echo curl_error ( $ch );
            curl_close ( $ch );
            exit ();
        }
        curl_close ( $ch );
        return $file_contents;
    }

    private function get_payment_id( $customer  ,$iban  ,$account_owner )
    {
        $res = $this->call_payment_api_json('post',$this->api_url,[
                    'customerId' =>  $customer->id,
                    'iban' => $iban  ,
                    'owner' =>   $account_owner
                ]);

        $result = json_decode($res); 
        if(  $result->paymentDataId )
        {
            return $result->paymentDataId ; 
        }
        return "0" ; 
    }
    // not used 

    private function find_customer($telephone)
    {
        // $customer = Customer::where('telephone',$telephone)->get();
        // if($customer){
        //     $this->register($step = $customer->last_step  ,  $customer );
        // }
    }
    private function save_to_draft($step, $customer)
    {
        // $response = file_get_contents('http://example.com/path/to/api/call?param1=5');
        // $response = json_decode($response);
        Session::flash('message', 
                'Your data in step '.$prevStep.
                ' has been saved in draft!'); 
        Session::flash('alert-class', 'alert-info'); 
        dd($customer);
    }

     

}
