<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [ 'firstname' , 'lastname' , 'telephone' ,
    	'address' ,'street' ,'house_no' , 'zip_code' , 'city' ,
    	'account_owner' , 'iban'   ];
}













