
# Welcome  to Wunderfleet task  #
It's a task for Junior/Senior position as Backend PHP Engineer - Dortmund (m/f).

___

I built a php laravel framework to make the task professionally with MVC structure. 
The aim of this task is to create  registrations  for customers with payments ID by calling API.

Wunderfleet Task db.sql is DB file.
----------

## 1. Describe possible performance optimizations for your Code.

I made the code as optimized as possible, usually you could inhance the performaces by 
	
	* Studying the code complicity,  o(n) nlog(n) is the best on complicity algorithms.
	* Tuning on sql queries
	* Using minimize the data transferring  between servers/clients.
	* Using webpack for compress JS/Css files to reduce the spaces in files and file spaces


### 2. Which things could be done better, than you’ve done it?

In my view,

	* I prefer to use frameworks and open-source libs as I used Laravel, it would be better if I used frontend framework like ( ReactJs, VueJs, or even Angular)
		I am still junior and in the learning process to improve my skills to use one of them instead of Pure Js or JQuery.
	* More user-friendly and User experience. ( in the previous short period I did not have time because I was travelling. 
	* The way of re-entering data. It would be better by having (login username got it by email) then use it to log in to the system and complete the data ( as a better and alternative way to save it in the session).
	   " Saving data in database instead of session ".
	
__________________

Some snapshoots for this task.

First Page in Website

![picture alt](https://bitbucket.org/mhd-twl/wunderfleettask/raw/196d8d2a8f0f3d2f3cda5da6eac19628a819c2fc/_snapshots/index.JPG "First Page in Website")

Registration Step 1

![picture alt](https://bitbucket.org/mhd-twl/wunderfleettask/raw/196d8d2a8f0f3d2f3cda5da6eac19628a819c2fc/_snapshots/reg1.JPG "Registration Step 1")

Registration Step 2

![picture alt](https://bitbucket.org/mhd-twl/wunderfleettask/raw/196d8d2a8f0f3d2f3cda5da6eac19628a819c2fc/_snapshots/reg2.JPG "Registration Step 2")

![picture alt](https://bitbucket.org/mhd-twl/wunderfleettask/raw/196d8d2a8f0f3d2f3cda5da6eac19628a819c2fc/_snapshots/reg22.JPG "Registration Step 2")

Registration Step 3

![picture alt](https://bitbucket.org/mhd-twl/wunderfleettask/raw/196d8d2a8f0f3d2f3cda5da6eac19628a819c2fc/_snapshots/reg3.JPG "Registration Step 3")

Registration Step 4/Success Page

![picture alt](https://bitbucket.org/mhd-twl/wunderfleettask/raw/196d8d2a8f0f3d2f3cda5da6eac19628a819c2fc/_snapshots/succ.JPG "Registration Step 4 / Success Page")

_______________________________________________________________________________________________________________________________________________________________________________
_______________________________________________________________________________________________________________________________________________________________________________
_______________________________________________________________________________________________________________________________________________________________________________



## Some info about laravel framework ##

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:
Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell):


## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
